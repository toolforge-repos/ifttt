# -*- coding: utf-8 -*-
"""
  Wikipedia channel for IFTTT
  ~~~~~~~~~~~~~~~~~~~~~~~~~~~

  Copyright 2015 Stephen LaPorte <stephen.laporte@gmail.com>
  Copyright 2023 Wikimedia Foundation and contributors

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.

"""

import pymysql.cursors
import toolforge


DEFAULT_HOURS = 1
DEFAULT_LANG = "en"
DEFAULT_LIMIT = 50
EMPTY_TUPLE = ()

CATEGORY_MEMBERS_QUERY = """
SELECT
    rc_title, rc_cur_id, rc_namespace, cl_timestamp
FROM recentchanges
    JOIN categorylinks ON rc_cur_id = cl_from
WHERE rc_timestamp >= DATE_FORMAT(
  DATE_SUB(NOW(), INTERVAL %(hours)s HOUR),
  '%%Y%%m%%d%%H%%I%%s'
)
  AND cl_to = %(category)s
GROUP BY rc_cur_id
ORDER BY rc_timestamp DESC
LIMIT %(limit)s
"""

ARTICLE_LIST_REVISIONS_QUERY = """
SELECT DISTINCT
    rc.rc_id, rc.rc_cur_id, rc.rc_title, rc.rc_timestamp, rc.rc_this_oldid,
    rc.rc_last_oldid, a.actor_name, rc.rc_old_len, rc.rc_new_len,
    c.comment_text
FROM recentchanges rc
    LEFT JOIN actor a ON rc.rc_actor = a.actor_id
    LEFT JOIN comment c ON rc.rc_comment_id = c.comment_id
WHERE rc.rc_title IN ({})
AND rc.rc_type = 0
AND rc.rc_timestamp >= DATE_FORMAT(
  DATE_SUB(NOW(), INTERVAL %s HOUR),
  '%%Y%%m%%d%%H%%I%%s'
)
ORDER BY rc.rc_id DESC
LIMIT %s
"""

CATEGORY_MEMBER_REVISIONS_QUERY = """
SELECT
    rc.rc_id, rc.rc_cur_id, rc.rc_title, rc.rc_timestamp, rc.rc_this_oldid,
    rc.rc_last_oldid, a.actor_name, rc.rc_old_len, rc.rc_new_len,
    c.comment_text
FROM recentchanges AS rc
    LEFT JOIN actor a ON rc.rc_actor = a.actor_id
    LEFT JOIN comment c ON rc.rc_comment_id = c.comment_id
    INNER JOIN categorylinks AS cl ON rc.rc_cur_id = cl.cl_from
WHERE cl.cl_to = %(category)s
  AND rc.rc_type = 0
  AND rc.rc_timestamp >= DATE_FORMAT(
    DATE_SUB(NOW(), INTERVAL %(hours)s HOUR),
    '%%Y%%m%%d%%H%%I%%s'
  )
  AND rc.rc_type = 0
GROUP BY rc.rc_this_oldid
ORDER BY rc.rc_id DESC
LIMIT %(limit)s
"""


def run_query(query, query_params, lang):
    db_title = lang + "wiki_p"
    connection = toolforge.connect(db_title)
    cursor = connection.cursor(pymysql.cursors.DictCursor)
    cursor.execute(query, query_params)
    ret = cursor.fetchall()
    if ret == EMPTY_TUPLE:
        # Work around fetchall returning a empty tuple for empty selects
        return []
    return ret


def get_category_members(
    category_name, lang=DEFAULT_LANG, hours=DEFAULT_HOURS, limit=DEFAULT_LIMIT
):
    query_params = {
        "hours": hours,
        "category": category_name.replace(" ", "_"),
        "limit": limit,
    }
    ret = run_query(CATEGORY_MEMBERS_QUERY, query_params, lang)
    return ret


def get_article_list_revisions(
    articles, lang=DEFAULT_LANG, hours=DEFAULT_HOURS, limit=DEFAULT_LIMIT
):
    # Add a placeholder for each article title
    query = ARTICLE_LIST_REVISIONS_QUERY.format(", ".join(["%s" for title in articles]))
    # Use positional rather than named arguments to make IN() clause
    # management easier to deal with. This does make mapping between the query
    # and the params more fragile, so pay attention to order when changeing
    # things.
    query_params = tuple([title.replace(" ", "_") for title in articles]) + (
        hours,
        limit,
    )
    ret = run_query(query, query_params, lang)
    return ret


def get_category_member_revisions(
    category_name, lang=DEFAULT_LANG, hours=DEFAULT_HOURS, limit=DEFAULT_LIMIT
):
    query_params = {
        "category": category_name.replace(" ", "_"),
        "hours": hours,
        "limit": limit,
    }
    ret = run_query(CATEGORY_MEMBER_REVISIONS_QUERY, query_params, lang)
    return ret
